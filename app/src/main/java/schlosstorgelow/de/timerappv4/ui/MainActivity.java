package schlosstorgelow.de.timerappv4.ui;

import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import it.sephiroth.android.library.tooltip.Tooltip;
import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.db.DBConnector;
import schlosstorgelow.de.timerappv4.db.DBHelper;
import schlosstorgelow.de.timerappv4.obj.Subject;

public class MainActivity extends AppCompatActivity implements SubjectFragment.OnSubjectInteractionListener {

    public static final String TAG = "TimerApp";
    private Toolbar toolbar;
    private SubjectFragment subjectFragment = new SubjectFragment();
    private AverageFragment averageFragment = new AverageFragment();
    private boolean debug = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // enable transition on lollipop and above
            supportRequestWindowFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("TimerApp v4");

        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (debug) {
            ft.add(R.id.content, subjectFragment, "sub");
            ft.add(R.id.content, averageFragment, "avg");
        } else {
            ft.add(R.id.content, averageFragment, "avg");
            ft.add(R.id.content, subjectFragment, "sub");
        }
        ft.commit();

        //Open the database at runtime
        DBHelper.open(this);
    }

    private void addSubject() {
        new MaterialDialog.Builder(this)
                .title("Add Subject")
                .inputRange(0, 40, getResources().getColor(R.color.accent))
                .input("Subject Name", null, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog materialDialog, CharSequence charSequence) {
                        DBHelper.createSubject(new Subject(charSequence.toString()));
                        //Toast.makeText(getApplicationContext(), "New Subject Added!", Toast.LENGTH_LONG).show();
                        refresh();
                    }
                }).show();
    }

    private void genDummyData() {
        Subject deutsch = new Subject("Deutsch");
        Subject englisch = new Subject("English");

        DBHelper.createSubject(deutsch);
        DBHelper.createSubject(englisch);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onRestart() {
        refresh();
        super.onRestart();
    }

    @Override
    protected void onPause() {
        refresh();
        super.onPause();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_setting:
//                Intent i = new Intent(this, TreeViewActivity.class);
//                i.putExtra("subject", DBHelper.readAllSubjects().get(1));
//                startActivity(i);
                return true;
            case R.id.show_tutorial:
               /* Tooltip.make(this, new Tooltip.Builder(101)
                        .anchor(fab, Tooltip.Gravity.LEFT)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 0)
                                //.activateDelay(800)
                        .showDelay(300)
                        .text("Click here to add a Subject!")
                        .withArrow(true)
                        .floatingAnimation(Tooltip.AnimationBuilder.SLOW).build()).show();*/
                Point p = new Point();
                p.set(990, 180);
                Tooltip.make(this, new Tooltip.Builder(101)
                        .anchor(p, Tooltip.Gravity.BOTTOM)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 0)
                                //.activateDelay(800)
                        .showDelay(300)
                        .text("Click here to add a new Subject!")
                        .withArrow(true)
                        .floatingAnimation(Tooltip.AnimationBuilder.SLOW).build()).show();
                Tooltip.make(this, new Tooltip.Builder(101)
                        .anchor(getSupportFragmentManager().findFragmentByTag("sub").getView().findViewById(R.id.subjectFragment_listView), Tooltip.Gravity.TOP)
                        .closePolicy(new Tooltip.ClosePolicy()
                                .insidePolicy(true, false)
                                .outsidePolicy(true, false), 0)
                                //.activateDelay(800)
                        .showDelay(300)
                        .text("Manage all your Subjects by Clicking on them!")
                        .withArrow(true)
                        .floatingAnimation(Tooltip.AnimationBuilder.SLOW).build()).show();
                return true;

            case R.id.delete_database:

                new MaterialDialog.Builder(this)
                        .title("Database Delete?")
                        .positiveText("OK, fuck it!")
                        .content("Want to Delete the whole Database?")
                        .negativeText("Holy shit, NO!")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                new DBConnector(getApplicationContext()).onDelete(DBHelper.getDB());
                                refresh();
                            }
                        })
                        .show();
                return true;
            case R.id.add_subject:
                addSubject();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void refresh() {
        averageFragment.refresh();
        subjectFragment.refresh();
    }

    public void onSubjectInteraction(Subject s) {
        Intent i = new Intent(this, TreeViewActivity.class);
        i.putExtra("parent", s);
        startActivity(i);
    }
}
