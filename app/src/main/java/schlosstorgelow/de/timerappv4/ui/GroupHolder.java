package schlosstorgelow.de.timerappv4.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unnamed.b.atv.model.TreeNode;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.db.DBHelper;
import schlosstorgelow.de.timerappv4.obj.Group;

/**
 * Created by Jan Max on 17.12.2015.
 */
public class GroupHolder extends TreeNode.BaseNodeViewHolder<Group> {
    private GroupHolderListener mListener;

    public GroupHolder(Context context) {
        super(context);
        if (context instanceof GroupHolderListener) {
            mListener = (GroupHolderListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement GroupHolderListener");
        }

    }

    @Override
    public View createNodeView(final TreeNode node, final Group g) {
        final LayoutInflater lf = LayoutInflater.from(context);
        final View v = lf.inflate(R.layout.tree_group, null, false);

        TextView name = (TextView) v.findViewById(R.id.tree_group_name);
        TextView nog = (TextView) v.findViewById(R.id.tree_group_NoG);
        TextView nom = (TextView) v.findViewById(R.id.tree_group_NoM);

        name.setText(g.getName());
        nog.setText(String.valueOf(DBHelper.readGroupsByParentID(g.getHierarchyId()).size()));
        nom.setText(String.valueOf(DBHelper.readMarksByParentID(g.getHierarchyId()).size()));

        ImageButton info = (ImageButton) v.findViewById(R.id.tree_group_info);
        final ImageView open = (ImageView) v.findViewById(R.id.tree_group_arrow);

        node.setClickListener(new TreeNode.TreeNodeClickListener() {
            boolean isOpen = false;

            @Override
            public void onClick(TreeNode node, Object value) {
                if (isOpen) {
                    open.setImageDrawable(mListener.getResources().getDrawable(R.drawable.ic_keyboard_arrow_right_24dp));
                    isOpen = false;
                } else {
                    open.setImageDrawable(mListener.getResources().getDrawable(R.drawable.ic_keyboard_arrow_down_24dp));
                    isOpen = true;
                }
            }
        });


        Resources r = mListener.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, node.getLevel() * (node.getLevel() > 1 ? r.getInteger(R.integer.tree_margin) : 0), r.getDisplayMetrics());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = px;
        v.setLayoutParams(params);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.openGroupUpdate(g);
            }
        });

        return v;
    }

    public interface GroupHolderListener {
        void showSnackbar(String s);

        void openGroupUpdate(Group group);

        Resources getResources();

        void startActivity(Intent i);
    }
}
