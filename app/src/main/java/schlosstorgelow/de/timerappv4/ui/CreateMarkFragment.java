package schlosstorgelow.de.timerappv4.ui;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.db.DBHelper;
import schlosstorgelow.de.timerappv4.obj.DBObject;
import schlosstorgelow.de.timerappv4.obj.Group;
import schlosstorgelow.de.timerappv4.obj.Mark;

public class CreateMarkFragment extends Fragment implements CreateActivity.CreateActivityListener {
    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private EditText name;
    private EditText value;
    private EditText weight;
    private EditText date;
    private TextView percentage;
    private TextInputLayout inputLayout_name;
    private TextInputLayout inputLayout_value;
    private TextInputLayout inputLayout_weight;
    private TextInputLayout inputLayout_date;
    private ImageButton datepicker;
    private DBObject parent;
    private double allWeightOfParentGroup;
    private Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener today = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            setDate(year, monthOfYear, dayOfMonth);
        }

    };

    public CreateMarkFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parent = (DBObject) getArguments().getSerializable("parent");
        if (parent == null) {
            throw new RuntimeException(this.toString() + " needs to be passed a parent!");
        }

        for (Group g : DBHelper.readGroupsByParentID(parent.getHierarchyId())) {
            allWeightOfParentGroup += g.getWeight();
        }
        for (Mark m : DBHelper.readMarksByParentID(parent.getHierarchyId())) {
            allWeightOfParentGroup += m.getWeight();
        }

        View v = inflater.inflate(R.layout.fragment_create_mark, container, false);
        name = (EditText) v.findViewById(R.id.editText_name);
        value = (EditText) v.findViewById(R.id.editText_value);
        weight = (EditText) v.findViewById(R.id.editText_weight);
        date = (EditText) v.findViewById(R.id.editDate_date);

        percentage = (TextView) v.findViewById(R.id.percentage);

        inputLayout_name = (TextInputLayout) v.findViewById(R.id.input_view_name);
        inputLayout_value = (TextInputLayout) v.findViewById(R.id.input_view_value);
        inputLayout_weight = (TextInputLayout) v.findViewById(R.id.input_view_weight);
        inputLayout_date = (TextInputLayout) v.findViewById(R.id.input_view_date);

        datepicker = (ImageButton) v.findViewById(R.id.datepicker);

        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(getContext(), today, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

        name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validateName();
                }
            }
        });
        value.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validateValue();
                }
            }
        });
        weight.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validateWeight();
                }
            }
        });

        date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    validateDate();
                }
            }
        });

        weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                double input = 0;
                if (!(s.length() == 0)) {
                    input = Double.parseDouble(weight.getText().toString());
                    percentage.setText(String.valueOf((double) Math.round((input / (allWeightOfParentGroup + input)) * 10000) / 100.00) + "%");
                }

            }
        });

        return v;
    }

    private void setDate(int year, int monthOfYear, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        date.setText(dateFormat.format(myCalendar.getTime()));
    }

    private boolean validateDate() {
        boolean inputOK = false;
        if (date.getText().length() == 0) {
            inputLayout_date.setErrorEnabled(true);
            inputLayout_date.setError("Date can't be empty!");
        } else {
            try {
                Date input = dateFormat.parse(date.getText().toString());
                setDate(input.getYear(), input.getMonth(), input.getDay());
                inputOK = true;
                inputLayout_date.setErrorEnabled(false);
            } catch (ParseException e) {
                inputLayout_date.setErrorEnabled(true);
                inputLayout_date.setError("Please enter Date in the format dd/MM/yyyy");
            }
        }
        return inputOK;
    }

    public boolean validateWeight() {
        boolean inputOK = false;
        double input = 0;
        if (weight.getText().length() == 0) {
            inputLayout_weight.setErrorEnabled(true);
            inputLayout_weight.setError("Weight can't be empty!");
        } else {
            input = Double.parseDouble(weight.getText().toString());
            if (input <= 0) {
                inputLayout_weight.setErrorEnabled(true);
                inputLayout_weight.setError("Weight must be greater that zero!");
            } else {
                inputLayout_weight.setErrorEnabled(false);
                inputOK = true;
            }
        }


        return inputOK;

    }

    public boolean validateValue() {
        boolean inputOK = false;
        double input = 0;
        if (value.getText().length() == 0) {
            inputLayout_value.setErrorEnabled(true);
            inputLayout_value.setError("Value can't be empty!");
        } else {
            input = Double.parseDouble(value.getText().toString());
            if (input > 15 || input < 0) {
                inputLayout_value.setErrorEnabled(true);
                inputLayout_value.setError("Value has to be in range from 0 to 15");
            } else {
                inputLayout_value.setErrorEnabled(false);
                inputOK = true;
            }
        }


        return inputOK;

    }

    public boolean validateName() {
        boolean inputOK = false;
        String input = name.getText().toString();
        if (input.length() == 0) {
            inputLayout_name.setErrorEnabled(true);
            inputLayout_name.setError("Name can't be empty!");
        } else if (input.length() > 40) {
            inputLayout_name.setErrorEnabled(true);
            inputLayout_name.setError("Name can't be longer that 40 Symbols!");
        } else {
            inputLayout_name.setErrorEnabled(false);
            inputOK = true;
        }

        return inputOK;
    }

    public boolean save(DBObject p) {

        if (validateName() && validateWeight() && validateValue()) {
            String _name = name.getText().toString();
            double _value = Double.parseDouble(value.getText().toString());
            double _weight = Double.parseDouble(weight.getText().toString());
            DBHelper.createMark(new Mark(p.getHierarchyId(), _name, _value, _weight, myCalendar.getTime()));
            return true;
        } else {
            return false;
        }
    }

    @Override
    public DBObject getParent() {
        return parent;
    }
}
