package schlosstorgelow.de.timerappv4.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Collection;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.db.DBHelper;
import schlosstorgelow.de.timerappv4.obj.Subject;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSubjectInteractionListener} interface
 * to handle interaction events.
 */
public class SubjectFragment extends Fragment {

    private OnSubjectInteractionListener mListener;
    private SubjectAdapter subjectAdapter;

    public SubjectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_subject, container, false);
        // Get the ListView
        ListView listView = (ListView) view.findViewById(R.id.subjectFragment_listView);
        subjectAdapter = new SubjectAdapter(getActivity());
        listView.setAdapter(subjectAdapter);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSubjectInteractionListener) {
            mListener = (OnSubjectInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSubjectInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void refresh() {
        subjectAdapter.clear();
        subjectAdapter.addAll(DBHelper.readAllSubjects());
        subjectAdapter.notifyDataSetChanged();
    }

    public interface OnSubjectInteractionListener {
        void onSubjectInteraction(Subject s);

        void refresh();
    }

    private class SubjectAdapter extends ArrayAdapter<Subject> {
        public SubjectAdapter(Context context) {
            super(context, R.layout.list_sub_view, DBHelper.readAllSubjects());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                v = LayoutInflater.from(getContext()).inflate(R.layout.list_sub_view, null);
            }
            final Subject s = getItem(position);
            if (s != null) {
                TextView title = (TextView) v.findViewById(R.id.title);
                TextView detail_1 = (TextView) v.findViewById(R.id.detail_1);
                TextView detail_2 = (TextView) v.findViewById(R.id.detail_2);

                ImageView img = (ImageView) v.findViewById(R.id.img_arrow);
                ImageButton delete = (ImageButton) v.findViewById(R.id.sub_fragment_delete);

                title.setText(s.getName());
                detail_1.setText("Average: " + s.getAverage());
                detail_2.setText("");
                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onSubjectInteraction(s);
                        mListener.refresh();
                    }
                });
                delete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new MaterialDialog.Builder(getContext())
                                .title("Delete " + s.getName() + "?")
                                .content("Are you sure to delete this Subject" +
                                        " and all its components?")
                                .positiveText("Yeah!")
                                .negativeText("Keep It!")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                        Log.d(MainActivity.TAG, String.valueOf(DBHelper.deleteSubject(s)));
                                        mListener.refresh();
                                    }
                                }).show();
                    }
                });
            }

            return v;
        }

        @Override
        public void addAll(Collection<? extends Subject> collection) {
            super.addAll(collection);
        }

        @Override
        public void clear() {
            super.clear();
        }
    }
}
