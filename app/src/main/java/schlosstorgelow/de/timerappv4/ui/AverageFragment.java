package schlosstorgelow.de.timerappv4.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.db.DBHelper;

/**
 * A simple {@link Fragment} subclass.
 */
public class AverageFragment extends Fragment {

    TextView average;
    TextView message;


    public AverageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_average, container, false);
        average = (TextView) v.findViewById(R.id.averageFragment_average);
        message = (TextView) v.findViewById(R.id.averageFragment_avgMessage);
        message.setText("");
        setAverage(DBHelper.getGlobalAvg());
        return v;
    }

    public void setAverage(double avg) {
        if (Double.isInfinite(avg) || Double.isNaN(avg)){
            avg = 0.0;
        }
        if (average != null){
            average.setText(String.valueOf(avg));
        }
    }

    public void setMessage(String msg) {
        message.setText(msg);
    }

    public void refresh() {
            setAverage(DBHelper.getGlobalAvg());
    }
}
