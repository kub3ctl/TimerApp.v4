package schlosstorgelow.de.timerappv4.ui;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.obj.DBObject;

public class CreateActivity extends AppCompatActivity {

    boolean isMarkFragment;
    private DBObject parent;
    private CreateMarkFragment createMarkFragment = new CreateMarkFragment();
    private CreateGroupFragment createGroupFragment = new CreateGroupFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        parent = (DBObject) getIntent().getSerializableExtra("parent");
        Bundle b = new Bundle();
        b.putSerializable("parent", parent);
        setTitle("Add to " + parent.getName());
        createGroupFragment.setArguments(b);
        createMarkFragment.setArguments(b);
        getSupportFragmentManager().beginTransaction().add(R.id.content, createMarkFragment, "content").commit();

        isMarkFragment = true;

        final Button add_mark = (Button) findViewById(R.id.add_mark_button);
        final Button add_group = (Button) findViewById(R.id.add_group_button);

        add_mark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isMarkFragment) {
                    add_mark.setBackgroundColor(getResources().getColor(R.color.secondaryAccent));
                    add_group.setBackgroundColor(getResources().getColor(R.color.accent));
                    getSupportFragmentManager().beginTransaction().replace(R.id.content, createMarkFragment, "content").commit();
                    isMarkFragment = true;
                } else {
                    //Already in MarkView
                }
            }
        });

        add_group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMarkFragment) {
                    add_mark.setBackgroundColor(getResources().getColor(R.color.accent));
                    add_group.setBackgroundColor(getResources().getColor(R.color.secondaryAccent));
                    getSupportFragmentManager().beginTransaction().replace(R.id.content, createGroupFragment, "content").commit();
                    isMarkFragment = false;
                } else {
                    //Already In GroupView
                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CreateActivityListener activityListener = (CreateActivityListener) getSupportFragmentManager().findFragmentByTag("content");
                if (activityListener.save(parent)) {
                    onBackPressed();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Please enter valid data!", Snackbar.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public interface CreateActivityListener {
        DBObject parent = null;

        boolean save(DBObject parent);

        DBObject getParent();
    }
}
