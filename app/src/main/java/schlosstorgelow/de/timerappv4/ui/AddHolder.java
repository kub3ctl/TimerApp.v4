package schlosstorgelow.de.timerappv4.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.unnamed.b.atv.model.TreeNode;

import schlosstorgelow.de.timerappv4.R;
import schlosstorgelow.de.timerappv4.obj.DBObject;

/**
 * Created by Jan Max on 17.12.2015.
 */
public class AddHolder extends TreeNode.BaseNodeViewHolder<DBObject> {
    private AddHolderListener mListener;

    public AddHolder(Context context) {
        super(context);
        if (context instanceof AddHolderListener) {
            mListener = (AddHolderListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement AddHolderListener!");
        }
    }

    @Override
    public View createNodeView(final TreeNode node, final DBObject parent) {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.list_add_view, null, false);
        Resources r = mListener.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, node.getLevel() * (node.getLevel() != 1 ? r.getInteger(R.integer.tree_margin) : 0), r.getDisplayMetrics());
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = px;
        view.setLayoutParams(params);
        view.findViewById(R.id.ripple_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, CreateActivity.class);
                i.putExtra("parent", parent);
                mListener.startActivity(i);
                node.setExpanded(true);
            }
        });
        return view;
    }

    public interface AddHolderListener {
        void startActivity(Intent i);

        Resources getResources();
    }
}
