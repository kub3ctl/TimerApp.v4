package schlosstorgelow.de.timerappv4.obj;

import android.annotation.TargetApi;
import android.os.Build;

import java.io.Serializable;

/**************************************************************************************************
 * Copyright (c) 2015. Jan Max Tiedemann                                                          *
 * Privates Internatsgymnasium Schloss Torgelow                                                   *
 * All Rights reserved. The usage of this Code                                                    *
 * is allowed under the GNU Licence                                                               *
 * For any further Questions: janmaxtiedemann@aol.com                                             *
 **************************************************************************************************/

public class Group implements Comparable<Group>, Serializable, DBObject {

    private long id;
    private long hierarchyId;
    private long parentId;
    private String name;
    private double weight;

    public Group(long parent_id, String name, double weight) {
        this.parentId = parent_id;
        this.name = name;
        this.weight = weight;
    }

    public Group() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getHierarchyId() {
        return hierarchyId;
    }

    public void setHierarchyId(long hierarchyId) {
        this.hierarchyId = hierarchyId;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }


    // FUNCTIONS

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int compareTo(Group another) {
        return name.compareTo(another.getName()) == 0 ? Long.compare(this.getId(), another.getId()) : name.compareTo(another.getName());
    }
}
