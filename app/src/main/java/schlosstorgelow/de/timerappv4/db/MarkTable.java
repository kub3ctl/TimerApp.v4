package schlosstorgelow.de.timerappv4.db;

/**************************************************************************************************
 * Copyright (c) 2015. Jan Max Tiedemann                                                          *
 * Privates Internatsgymnasium Schloss Torgelow                                                   *
 * All Rights reserved. The usage of this Code                                                    *
 * is allowed under the GNU Licence                                                               *
 * For any further Questions: janmaxtiedemann@aol.com                                             *
 **************************************************************************************************/

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

class MarkTable {

    //Database Table
    public static final String TABLE = "mark";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_VALUE = "value";
    public static final String COLUMN_WEIGHT = "weight";
    public static final String COLUMN_DATE = "date";

    public static final String[] ALL_COLUMNS = {COLUMN_ID, COLUMN_NAME, COLUMN_VALUE, COLUMN_WEIGHT, COLUMN_DATE};

    // TABLE NUMBERS
    public static final int COL_NO_ID = 0;
    public static final int COL_NO_NAME = 1;
    public static final int COL_NO_VALUE = 2;
    public static final int COL_NO_WEIGHT = 3;
    public static final int COL_NO_DATE = 4;

    //Database Create
    private static final String DATABASE_CREATE = "CREATE TABLE "
            + TABLE
            + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
            + COLUMN_NAME + " VARCHAR(45) NOT NULL, "
            + COLUMN_VALUE + " DOUBLE NOT NULL, "
            + COLUMN_WEIGHT + " DOUBLE NOT NULL, "
            + COLUMN_DATE + "  DATE "
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.v(MarkTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);

    }

    public static void onDelete(SQLiteDatabase database) {
        Log.v(GroupTable.class.getName(), "Will reset database table:" + TABLE + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }

    public static void onDowngrade(SQLiteDatabase database, int oldVersion,
                                   int newVersion) {
        Log.v(MarkTable.class.getName(), "Downgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }
}