package schlosstorgelow.de.timerappv4.db;

/**
 * Copyright (c) 2015. Jan Max Tiedemann                                                          *
 * Privates Internatsgymnasium Schloss Torgelow                                                   *
 * All Rights reserved. The usage of this Code                                                    *
 * is allowed under the GNU Licence                                                               *
 * For any further Questions: janmaxtiedemann@aol.com                                             *
 * <p/>
 * Created by Jan Max on 08.10.2015.
 * <p/>
 * Created by Jan Max on 08.10.2015.
 */

/**
 * Created by Jan Max on 08.10.2015.
 */

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

class GroupTable {

    //Database Table
    public static final String TABLE = "groups";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WEIGHT = "weight";

    public static final String[] ALL_COLUMNS = {COLUMN_ID, COLUMN_NAME, COLUMN_WEIGHT};

    // TABLE NUMBERS
    public static final int COL_NO_ID = 0;
    public static final int COL_NO_NAME = 1;
    public static final int COL_NO_WEIGHT = 2;

    //Database Create
    private static final String DATABASE_CREATE = "CREATE TABLE "
            + TABLE
            + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
            + COLUMN_NAME + " VARCHAR(45) NOT NULL, "
            + COLUMN_WEIGHT + " DOUBLE NOT NULL "
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.v(GroupTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);

    }

    public static void onDelete(SQLiteDatabase database) {
        Log.v(GroupTable.class.getName(), "Will reset database table:" + TABLE + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }

    public static void onDowngrade(SQLiteDatabase database, int oldVersion,
                                   int newVersion) {
        Log.v(GroupTable.class.getName(), "Downgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }
}