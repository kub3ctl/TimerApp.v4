package schlosstorgelow.de.timerappv4.db;

/**************************************************************************************************
 * Copyright (c) 2015. Jan Max Tiedemann                                                          *
 * Privates Internatsgymnasium Schloss Torgelow                                                   *
 * All Rights reserved. The usage of this Code                                                    *
 * is allowed under the GNU Licence                                                               *
 * For any further Questions: janmaxtiedemann@aol.com                                             *
 **************************************************************************************************/

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Subclass of the Connector Object
 * Establishes connection between the low and top level database access
 * Creates and if necessary updates the found database file to the current version
 * For data safety a backup of the database is saved to the disk of the phone
 * <p/>
 * Created by JanMax on 28.06.2015.
 */

public class DBConnector extends SQLiteOpenHelper {

    private static final Integer DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "timerapp.db";

    public DBConnector(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        SubjectTable.onCreate(db);
        GroupTable.onCreate(db);
        MarkTable.onCreate(db);
        HierarchyTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        SubjectTable.onUpgrade(db, oldVersion, newVersion);
        GroupTable.onUpgrade(db, oldVersion, newVersion);
        MarkTable.onUpgrade(db, oldVersion, newVersion);
        HierarchyTable.onUpgrade(db, oldVersion, newVersion);
    }

    public void onDelete(SQLiteDatabase db) {
        SubjectTable.onDelete(db);
        GroupTable.onDelete(db);
        MarkTable.onDelete(db);
        HierarchyTable.onDelete(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        SubjectTable.onDowngrade(db, oldVersion, newVersion);
        GroupTable.onDowngrade(db, oldVersion, newVersion);
        MarkTable.onDowngrade(db, oldVersion, newVersion);
        HierarchyTable.onDowngrade(db, oldVersion, newVersion);
    }
}
