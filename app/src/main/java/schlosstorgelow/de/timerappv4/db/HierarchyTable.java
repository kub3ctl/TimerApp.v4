package schlosstorgelow.de.timerappv4.db;

/**************************************************************************************************
 * Copyright (c) 2015. Jan Max Tiedemann                                                          *
 * Privates Internatsgymnasium Schloss Torgelow                                                   *
 * All Rights reserved. The usage of this Code                                                    *
 * is allowed under the GNU Licence                                                               *
 * For any further Questions: janmaxtiedemann@aol.com                                             *
 **************************************************************************************************/

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Jan Max on 08.10.2015.
 */
class HierarchyTable {

    //Database Table
    public static final String TABLE = "hierarchy";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_RELATED_ID = "related_id";
    public static final String COLUMN_PARENT_ID = "parent_id";

    public static final String[] ALL_COLUMNS = {COLUMN_ID, COLUMN_TYPE, COLUMN_RELATED_ID, COLUMN_PARENT_ID};

    // Type Numbers
    public static final int TYPE_SUBJECT = 0;
    public static final int TYPE_GROUP = 1;
    public static final int TYPE_MARK = 2;


    // TABLE NUMBERS
    public static final int COL_NO_ID = 0;
    public static final int COL_NO_TYPE = 1;
    public static final int COL_NO_RELATED_ID = 2;
    public static final int COL_NO_PARENT_ID = 3;

    //Database Create
    private static final String DATABASE_CREATE = "CREATE TABLE "
            + TABLE
            + "("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
            + COLUMN_TYPE + " INTEGER NOT NULL, "
            + COLUMN_RELATED_ID + " INTEGER NOT NULL, "
            + COLUMN_PARENT_ID + " INTEGER "
            + ");";

    public static void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase database, int oldVersion,
                                 int newVersion) {
        Log.v(SubjectTable.class.getName(), "Upgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);

    }

    public static void onDelete(SQLiteDatabase database) {
        Log.v(GroupTable.class.getName(), "Will reset database table:" + TABLE + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }

    public static void onDowngrade(SQLiteDatabase database, int oldVersion,
                                   int newVersion) {
        Log.v(SubjectTable.class.getName(), "Downgrading database from version "
                + oldVersion + " to " + newVersion
                + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(database);
    }
}
